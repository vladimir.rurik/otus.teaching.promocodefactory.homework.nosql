﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Redis.Preferences.Requests;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using StackExchange.Redis;
using System.Collections.Generic;

namespace Otus.Redis.Preferences.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreferencesController : ControllerBase
    {
        private readonly IDatabase _db;
        private readonly IConnectionMultiplexer _mux;
        public PreferencesController(IConnectionMultiplexer mux)
        {
            _mux = mux;
            _db = mux.GetDatabase();
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAllPreferences()
        {
            IEnumerable<RedisKey> redisKeys = this._mux.GetServers().First().Keys();
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (RedisKey redisKey in redisKeys) 
            {
                RedisValue redisValue = _db.StringGet(new RedisKey(redisKey));
                dictionary.Add(redisKey.ToString(), redisValue.ToString());
            }
            return Ok(dictionary);
        }

        [HttpGet]
        public async Task<IActionResult> GetPreference(string guidStr)
        {
            RedisValue redisValue = _db.StringGet(new RedisKey(guidStr));
            if (!redisValue.HasValue)
                return NotFound(guidStr);
            return Ok(redisValue.ToString());
        }

        [HttpPost]
        public async Task<IActionResult> AddPreference([FromBody] PreferenceRequest preference)
        {
            string redisKey = Guid.NewGuid().ToString();
            Task<bool> status = _db.StringSetAsync(redisKey, preference.Name);
            if (status.IsCompleted)
            { BadRequest(redisKey); };
            return Ok(redisKey);
        }
    }
}
