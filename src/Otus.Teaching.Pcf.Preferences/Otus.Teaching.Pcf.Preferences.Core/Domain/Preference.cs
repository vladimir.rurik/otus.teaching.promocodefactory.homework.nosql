﻿namespace Otus.Teaching.Pcf.Preferences.Core.Domain
{
    public class Preference : BaseEntity
    {
        public string Name { get; set; }
    }
}