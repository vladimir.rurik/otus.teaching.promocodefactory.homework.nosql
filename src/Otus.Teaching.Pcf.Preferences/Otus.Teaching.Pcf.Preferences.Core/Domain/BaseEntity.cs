﻿using System;

namespace Otus.Teaching.Pcf.Preferences.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}