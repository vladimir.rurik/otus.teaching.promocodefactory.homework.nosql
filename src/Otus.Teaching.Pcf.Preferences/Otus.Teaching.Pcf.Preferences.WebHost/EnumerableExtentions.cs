﻿using System.Collections.Generic;

namespace Otus.Teaching.Pcf.Preferences.WebHost
{
    public static class EnumerableExtentions
    {
        public static int GetHashCode<T>(this IEnumerable<T> values)
        {
            unchecked
            {
                int hash = 19;
                foreach (var value in values)
                {
                    hash = hash * 31 + value.GetHashCode();
                }
                return hash;
            }
        }
    }
}