using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Preferences.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Preferences.DataAccess;
using Otus.Teaching.Pcf.Preferences.DataAccess.Data;
using Otus.Teaching.Pcf.Preferences.DataAccess.Repositories;
using Otus.Teaching.Pcf.Preferences.WebHost.Middleware;

namespace Otus.Teaching.Pcf.Preferences.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddDbContext<DataContext>(x =>
            {
                x.UseSqlite("Filename=PromocodeFactoryPreferencesrDb.sqlite");
            });
            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory Customers Preferences API Doc";
                options.Version = "1.0";
            });
            services.AddLogging();
            services.AddDistributedRedisCache(options =>
            {
                options.Configuration = "promocode-factory-preferences-cache";
                options.InstanceName = "Caching";
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseResponseMetric();

            app.UseCaching();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            dbInitializer.InitializeDb();
        }
    }
}
