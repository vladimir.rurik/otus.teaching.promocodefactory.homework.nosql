﻿using Otus.Teaching.Pcf.Preferences.Core.Domain;
using System;

namespace Otus.Teaching.Pcf.Preferences.WebHost.Models
{
    public class PreferenceResponse
    {
        public PreferenceResponse() { }

        public PreferenceResponse(Preference preference)
        {
            Id = preference.Id;
            Name = preference.Name;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}