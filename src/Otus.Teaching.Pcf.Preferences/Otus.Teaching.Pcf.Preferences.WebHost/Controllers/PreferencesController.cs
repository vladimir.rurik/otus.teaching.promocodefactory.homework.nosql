﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.Preferences.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Preferences.Core.Domain;
using Otus.Teaching.Pcf.Preferences.WebHost.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Preferences.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IDistributedCache _distributedCache;

        public PreferencesController(
            IRepository<Preference> preferencesRepository,
            IDistributedCache distributedCache)
        {
            _preferencesRepository = preferencesRepository;
            _distributedCache = distributedCache;
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            var preferences = await _preferencesRepository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить предпочтение по id
        /// </summary>
        /// <param name="id">Id предпочтения, например <example>ef7f299f-92d7-459f-896e-078ed53ef99c</example></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PreferenceResponse>> GetPreferenceAsync(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            var response = new PreferenceResponse(preference);

            return Ok(response);
        }

        /// <summary>
        /// Получить несколько предпочтений по id
        /// </summary>
        /// <param name="ids">Id предпочтения, например 
        /// <example>
        /// [ "ef7f299f-92d7-459f-896e-078ed53ef99c",
        ///   "c4bda62e-fc74-4256-a956-4760b3858cbd" ]
        /// </example></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<IEnumerable<PreferenceResponse>>> GetPreferenceAsync(IEnumerable<Guid> ids)
        {
            var hashCode = ids.GetHashCode<Guid>();
            var key = $"caching_{HttpContext.Request.Path}_{hashCode}";
            var cache = await _distributedCache.GetAsync(key);
            var response = new List<PreferenceResponse>(ids.Count());
            if (cache != null)
            {
                response = JsonSerializer.Deserialize<List<PreferenceResponse>>(cache);
                HttpContext.Response.Headers.Append("Source", "Cache");
            }
            else
            {
                response = (await _preferencesRepository.GetRangeByIdsAsync(ids.ToList()))
                    .Select(p => new PreferenceResponse(p))
                    .ToList();

                var toCache = JsonSerializer.Serialize(response);
                await _distributedCache.SetStringAsync(key, toCache, new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromSeconds(50)
                });
            }

            if (response.Count() == 0)
                return NotFound();

            return Ok(response);
        }
    }
}