﻿using System;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.IntegrationTests.Data;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests
{
    public class DatabaseFixture: IDisposable
    {
        private readonly MongoTestDbInitializer _mongoTestDbInitializer;
        
        public DatabaseFixture()
        {
            MongoUrl = new MongoUrl("mongodb://mongo:docker@localhost:5017");
            //MongoUrl = new MongoUrl("mongodb://mongo:docker@promocode-factory-administration-db:27017");

            _mongoTestDbInitializer= new MongoTestDbInitializer(MongoUrl);
            _mongoTestDbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            _mongoTestDbInitializer.CleanDb();
        }

        public MongoUrl MongoUrl { get; internal set; }
    }
}