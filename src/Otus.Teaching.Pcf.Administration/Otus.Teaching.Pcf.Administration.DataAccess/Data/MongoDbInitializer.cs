﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private IMongoDatabase _mongoDatabase;

        public MongoDbInitializer(MongoUrl mongoUrl)
        {
            var mongoClient = new MongoClient(mongoUrl);

            _mongoDatabase = mongoClient.GetDatabase("AdministrationDb");
        }

        public void InitializeDb()
        {
            var employees = _mongoDatabase.GetCollection<Employee>(nameof(Employee));
            if (employees.CountDocuments(_ => true) == 0)
                employees.InsertMany(FakeDataFactory.Employees);

            var roles = _mongoDatabase.GetCollection<Role>(nameof(Role));
            if (roles.CountDocuments(_ => true) == 0)
                roles.InsertMany(FakeDataFactory.Roles);
        }
    }
}