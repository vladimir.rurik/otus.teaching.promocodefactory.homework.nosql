﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

using System.Net;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class PreferencesGateway
        : IPreferencesGateway
    {
        private readonly HttpClient _httpClient;

        public PreferencesGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Preference> GetPreferenceByIdAsync(Guid preferenceId)
        {
            var response = await _httpClient.GetAsync($"api/v1/preferences/{preferenceId}");

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }

            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<Preference>();
        }
    }
}