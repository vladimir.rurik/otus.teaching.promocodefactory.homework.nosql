﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface IPreferencesGateway
    {
        Task<Preference> GetPreferenceByIdAsync(Guid id);
    }
}
