﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    /// <example>
    /// {
    ///     "serviceInfo": "OTUS Service inforamation",
    ///     "partnerId": "f766e2bf-340a-46ea-bff3-f1700b435895",
    ///     "promoCode": "OTUS",
    ///     "preferenceId": "ef7f299f-92d7-459f-896e-078ed53ef99c",
    ///     "beginDate": "1970-01-01",
    ///     "endDate": "2070-01-01"
    /// }
    /// </example>
    public class GivePromoCodeRequest
    {
        public string ServiceInfo { get; set; }

        public Guid PartnerId { get; set; }

        public string PromoCode { get; set; }

        public Guid PreferenceId { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }
    }
}