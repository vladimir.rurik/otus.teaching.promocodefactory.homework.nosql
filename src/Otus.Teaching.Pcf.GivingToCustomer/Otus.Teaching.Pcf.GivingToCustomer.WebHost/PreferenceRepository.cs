﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Json;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    internal class PreferenceRepository : IBaseRepository<Preference>
    {
        private HttpClient client;
        private string connectNoSQL;

        public PreferenceRepository(IConfiguration configuration) 
        {
            this.client = new HttpClient();
            this.connectNoSQL = configuration.GetConnectionString("UrlNoSQL");
        }

        Task IBaseRepository<Preference>.AddAsync(Preference entity)
        {
            using var response = this.client.PostAsJsonAsync(this.connectNoSQL, entity);
            Task<Preference> preference = response.Result.Content.ReadFromJsonAsync<Preference>();
            return Task.FromResult(preference.Result);
        }

        Task IBaseRepository<Preference>.DeleteAsync(Preference entity)
        {
            using var response = this.client.PostAsJsonAsync(this.connectNoSQL, entity);
            Task<Preference> preference = response.Result.Content.ReadFromJsonAsync<Preference>();
            return Task.FromResult(preference.Result);
        }

        Task<IEnumerable<Preference>> IBaseRepository<Preference>.GetAllAsync()
        {
            using var response = this.client.GetAsync(this.connectNoSQL + "/all");
            Task <Dictionary<Guid, string>> preferences = response.Result.Content.ReadFromJsonAsync<Dictionary<Guid, string>>();
            List<Preference> preferencesList = new List<Preference>();
            foreach (KeyValuePair<Guid, string> entry in preferences.Result)
                preferencesList.Add(new Preference { Id = entry.Key, Name = entry.Value });;
            return Task.FromResult(preferencesList as IEnumerable<Preference>);
        }
    }
}