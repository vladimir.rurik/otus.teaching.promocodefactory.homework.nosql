﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class PreferencesGateway
        : IPreferencesGateway
    {
        private readonly HttpClient _httpClient;

        public PreferencesGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Preference> GetPreferenceByIdAsync(Guid preferenceId)
        {
            var response = await _httpClient.GetAsync($"api/v1/preferences/{preferenceId}");

            response.EnsureSuccessStatusCode();
            return await response.Content.ReadFromJsonAsync<Preference>();
        }

        public async Task<IEnumerable<Preference>> GetRangeByIdsAsync(IEnumerable<Guid> preferenceIds)
        {
            if (preferenceIds == null)
            {
                throw new ArgumentNullException(nameof(preferenceIds));
            }
            if (!preferenceIds.Any())
            {
                return new List<Preference>();
            }
            
            var response = await _httpClient.PostAsJsonAsync($"api/v1/preferences/", preferenceIds);

            response.EnsureSuccessStatusCode();
            return await response.Content.ReadFromJsonAsync<IEnumerable<Preference>>();
        }
    }
}