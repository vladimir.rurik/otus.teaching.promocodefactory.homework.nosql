﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public string Email { get; set; }

        public List<Guid> PreferenceIds { get; set; } = new List<Guid>();
        public List<Guid> PromoCodeIds { get; set; } = new List<Guid>();
    }
}