﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways
{
    public interface IPreferencesGateway
    {
        Task<Preference> GetPreferenceByIdAsync(Guid id);
        Task<IEnumerable<Preference>> GetRangeByIdsAsync(IEnumerable<Guid> preferenceIds);
    }
}
