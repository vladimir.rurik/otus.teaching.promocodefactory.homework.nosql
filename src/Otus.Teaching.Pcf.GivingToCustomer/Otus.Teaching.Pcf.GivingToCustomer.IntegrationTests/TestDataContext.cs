﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class TestDataContext : DataContext
    {
        public TestDataContext(string connection) : base(connection, "GivingToCustomerTestDb")
        {
        }
    }
}