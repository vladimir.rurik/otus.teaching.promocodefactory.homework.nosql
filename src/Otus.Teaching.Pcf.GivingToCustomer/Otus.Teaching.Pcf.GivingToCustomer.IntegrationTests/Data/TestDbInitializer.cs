﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;
using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data
{
    public class TestDbInitializer
        : IDbInitializer
    {
        private TestDataContext _context;

        public TestDbInitializer(TestDataContext context)
        {
            _context = context;
        }

        public void InitializeDb()
        {
            var customers = _context.GetCollection<Customer>();
            if (customers.CountDocuments(_ => true) == 0)
                customers.InsertMany(FakeDataFactory.Customers);
        }

        internal void CleanDb()
        {
            _context.DropCollection<Customer>();
        }
    }
}