﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Integration;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Xunit;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Components.WebHost.Controllers
{
    [Collection(EfDatabaseCollection.DbCollection)]
    public class CustomersControllerTests: IClassFixture<DatabaseFixture>
    {
        private readonly CustomersController _customersController;
        private readonly MongoRepository<Customer> _customerRepository;
        private readonly PreferencesGateway _preferenceGateway;
        private readonly MongoRepository<PromoCode> _promoCodeRepository;

        public CustomersControllerTests(DatabaseFixture databaseFixture)
        {
            _customerRepository = new MongoRepository<Customer>(databaseFixture.DbContext);
            _promoCodeRepository = new MongoRepository<PromoCode>(databaseFixture.DbContext);
            _preferenceGateway = new PreferencesGateway(new HttpClient 
            {
                BaseAddress = new Uri("http://localhost:8094")
                //BaseAddress = new Uri("http://promocode-factory-preferences-api")
            });
            
            _customersController = new CustomersController(
                _customerRepository, 
                _preferenceGateway,
                _promoCodeRepository);
        }
        
        [Fact]
        public async Task CreateCustomerAsync_CanCreateCustomer_ShouldCreateExpectedCustomer()
        {
            //Arrange 
            var preferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c");
            var request = new CreateOrEditCustomerRequest()
            {
                Email = "some@mail.ru",
                FirstName = "Иван",
                LastName = "ПетровTEST",
                PreferenceIds = new List<Guid>()
                {
                    preferenceId
                }
            };

            //Act
            var result = await _customersController.CreateCustomerAsync(request);
            var actionResult = result.Result as CreatedAtActionResult;
            var id = (Guid)actionResult.Value;
            
            //Assert
            var actual = await _customerRepository.GetByIdAsync(id);
            
            actual.Email.Should().Be(request.Email);
            actual.FirstName.Should().Be(request.FirstName);
            actual.LastName.Should().Be(request.LastName);
            actual.PreferenceIds.Should()
                .ContainSingle()
                .And
                .Contain(x => x == preferenceId);
        }
    }
}