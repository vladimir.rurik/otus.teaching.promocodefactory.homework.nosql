﻿using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class DataContext : DbContext
    {
        private static readonly Dictionary<Type, string> _dbEntityTypeTableNamePairs;

        private IMongoDatabase GivingToCustomerDb { get; }

        static DataContext()
        {
            _dbEntityTypeTableNamePairs = new Dictionary<Type, string>
            {
                { typeof(Customer), "Customers"},
                { typeof(PromoCode), "PromoCodes"},
                { typeof(Preference), "Preferences"}
            };
        }

        public DataContext(string connection, string databaseName = null)
        {
            if (databaseName == null)
                databaseName = nameof(GivingToCustomerDb);
            GivingToCustomerDb = new MongoClient(connection).GetDatabase(databaseName);
        }

        public IMongoCollection<T> GetCollection<T>() where T : BaseEntity
            => GivingToCustomerDb.GetCollection<T>(_dbEntityTypeTableNamePairs.GetValueOrDefault(typeof(T)));

        public void DropCollection<T>()
        {
            GivingToCustomerDb.DropCollection(_dbEntityTypeTableNamePairs.GetValueOrDefault(typeof(T)));
        }
    }
}