﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private DataContext _context;

        public MongoDbInitializer(DataContext context)
        {
            _context = context;
        }

        public void InitializeDb()
        {
            var customers = _context.GetCollection<Customer>();
            if (customers.CountDocuments(_ => true) == 0)
                customers.InsertMany(FakeDataFactory.Customers);
        }
    }
}