﻿using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly IMongoCollection<T> _collection;

        public MongoRepository(DataContext context)
        {
            _collection = context.GetCollection<T>();
        }

        public async Task AddAsync(T entity)
            => await _collection.InsertOneAsync(entity);

        public async Task DeleteAsync(T entity)
            => await _collection.DeleteOneAsync(x => x.Id == entity.Id);

        public async Task<IEnumerable<T>> GetAllAsync()
            => await _collection.Find(_ => true).ToListAsync();

        public async Task<T> GetByIdAsync(Guid id)
            => await _collection.Find(x => x.Id == id).FirstOrDefaultAsync();

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
            => await _collection.Find(predicate).FirstOrDefaultAsync();

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
            => await _collection.Find(x => ids.Contains(x.Id)).ToListAsync();

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
            => await _collection.Find(predicate).ToListAsync();

        public async Task UpdateAsync(T entity)
            => await _collection.ReplaceOneAsync(x => x.Id == entity.Id, entity);
    }
}